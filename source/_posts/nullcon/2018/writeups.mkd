---
layout: post
title: "nullcom HackIM 2018 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - web
date: 2018/02/14
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By        | Version | Comment
| ---       | ---     | ---
| noraj     | 1.0     | Creation

### CTF

- **Name** : nullcom HackIM 2018
- **Website** : [ctf.nullcon.net](http://ctf.nullcon.net/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/566)

## 200 - Web2 - Web

> Hidden in Plain Sight
>
> http://34.201.73.166/

Using [nikto](https://github.com/sullo/nikto), I found a git repository http://34.201.73.166/.git/ exposed.

Then with [GitTools](https://github.com/internetwache/GitTools) I dumped the repository:

```
$ ~/CTF/tools/GitTools/Dumper/gitdumper.sh http://34.201.73.166/.git/ repo
###########
# GitDumper is part of https://github.com/internetwache/GitTools
#
# Developed and maintained by @gehaxelt from @internetwache
#
# Use at your own risk. Usage might be illegal in certain circumstances.
# Only for educational purposes!
###########


[*] Destination folder does not exist
[+] Creating repo/.git/
[+] Downloaded: HEAD
[-] Downloaded: objects/info/packs
[+] Downloaded: description
[+] Downloaded: config
[+] Downloaded: COMMIT_EDITMSG
[+] Downloaded: index
[-] Downloaded: packed-refs
[+] Downloaded: refs/heads/master
[-] Downloaded: refs/remotes/origin/HEAD
[-] Downloaded: refs/stash
[+] Downloaded: logs/HEAD
[+] Downloaded: logs/refs/heads/master
[-] Downloaded: logs/refs/remotes/origin/HEAD
[-] Downloaded: info/refs
[+] Downloaded: info/exclude
[+] Downloaded: objects/4b/95ce4491c7b505cf37ce8f38c95da668d9ad78
[-] Downloaded: objects/00/00000000000000000000000000000000000000
[+] Downloaded: objects/59/173b32d8802b3109763b8804076b96410c7328
[+] Downloaded: objects/34/cdc16028467cff91bb53487124e7638aab702b
[+] Downloaded: objects/de/e3c7fea3dec33caa3ef25110ccdd466b2aa225
[+] Downloaded: objects/09/6a77768a37271151786b67af92c2ca82760dff
[+] Downloaded: objects/49/c042bea82f729d4aa2f8e664942d1372a58018
[+] Downloaded: objects/71/4ef812ab99c0cc6bef4a772b5360fbfbe4b367
[+] Downloaded: objects/2a/a3f234d2ce991a9ddea8b216192c4c8c8217f6
[+] Downloaded: objects/6c/1a890b7cd36c8cec010dc20a2d9cf5a78411c4
[+] Downloaded: objects/dc/b7e71bfc04cf9f737b7b671aed07d87b4e5fb6
[+] Downloaded: objects/d0/e6ad36b77a2c8f9ba6708c995f1cb830b9e7fa
[+] Downloaded: objects/f3/3993193d51b645f99d63497ae7265820e05eda
[+] Downloaded: objects/8e/8d3ce7a602c71a2d7f0064a873dbe411e9a04e
[+] Downloaded: objects/b5/a144fb1fcf2acdcd5db2ac0725ed2679aa06aa
[+] Downloaded: objects/59/ba645070811b01a63dd8f8af89a65b21408643
[+] Downloaded: objects/48/38e77b2bb4655d0b46165ec6473460dc90b4dd
[-] Downloaded: objects/3e/90c63922fa145442bb58d18b62af6c21717fee
[+] Downloaded: objects/2f/e7e986096174eaa215846ae64ea83409594840
[+] Downloaded: objects/d8/d10cc949bd91efe792a72a119c796bbdb3dfc6
```

Now let's see what was changed:

```
$ git --no-pager log --oneline 
4b95ce4 (HEAD -> master) Corrected typo
49c042b Removing files.
096a777 Adding any other additional files
dee3c7f Added background image
34cdc16 Added CSS file for static content.
59173b3 Adding primary static HTML file
```

See files that were removed:

```
$ git checkout 096a777
Previous HEAD position was 49c042b Removing files.
HEAD is now at 096a777 Adding any other additional files

$ ls -R
.:
3e90c63922fa145442bb58d18b62af6c21717fee  header.jpg  index.html  style.css

./3e90c63922fa145442bb58d18b62af6c21717fee:
index.php  style.css
```

So `3e90c63922fa145442bb58d18b62af6c21717fee/index.php` is accessible and its content is:

```php
<html>
    <head>
        <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
    </head>
    <body>
    <form class="login" method="post">
    <h1 class="login-title">Login for flag</h1>
        <input name="user" id="user" type="text" class="login-input" placeholder="Username" autofocus>
        <input name="pass" id="pass" type="password" class="login-input" placeholder="Password">
        <input type="submit" value="Lets Go" class="login-button">


  <?php
error_reporting(0);
$FLAG = readfile('/var/flags/level1.txt');
if (!empty($_POST['user']) && !empty($_POST['pass'])) {
    if(checklogin($_POST['user'],$_POST['pass'])){
        echo "<font style=\"color:#FF0000\"><h3>The flag is: $FLAG</h3><br\></font\>";
    }else{
        echo "<br /><font style=\"color:#FF0000\">Invalid credentials! Please try again!<br\></font\>";
    }
}


function checklogin($u,$p)
{
    if (($u) === "passwordisinrockyou" && crc32($p) == "550274426"){ //
        return true;
        }
    }
?>
</form>

</body>
</html>
```

Display the deciaml crc32 in hexadecimal:

```
$ printf %x '550274426'
20cc857a
```

By the way the algorithm used by `crc32()` in php is **Crc32b php**.

So I asked [md5hashing.net](https://md5hashing.net/hash/crc32b_php/20cc857a) if it was knowing the password and it was. That's faster than bruteforcing it. So the password is *trumpet*.

After authentication we get the flag: `hackim18{'SeCuRiTy-MisConfiGuraTionS-ArE-Bad'}`.

---
layout: post
title: "SwampCTF 2019 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - stego
date: 2019/04/07
thumbnail: /images/ctf.png
authorId: noraj
toc: true
---
## Information

### CTF

- **Name** : SwampCTF 2019
- **Website** : [swampctf.com](https://swampctf.com/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/779)

## 50 - Leap of Faith - Stego

> “You have to let it all go, Neo. Fear, doubt, and disbelief. Free your mind (and your stego tools).” - Morpheus, probably
>
> -= Challenge by P4PA_0V3RL0RD =-

![](https://i.imgur.com/8CVJZAh.jpg)

```
$ exiftool leap_of_faith.jpeg 
ExifTool Version Number         : 11.30
File Name                       : leap_of_faith.jpeg
Directory                       : .
File Size                       : 41 kB
File Modification Date/Time     : 2019:04:07 00:18:02+02:00
File Access Date/Time           : 2019:04:07 00:18:18+02:00
File Inode Change Date/Time     : 2019:04:07 00:18:02+02:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Little-endian (Intel, II)
X Resolution                    : 72
Y Resolution                    : 72
Resolution Unit                 : inches
Modify Date                     : 2019:04:04 09:22:27
Exif Version                    : 0210
Date/Time Original              : 2019:04:04 09:22:27
Flashpix Version                : 0100
Color Space                     : Uncalibrated
Thumbnail Offset                : 226
Thumbnail Length                : 25217
Image Width                     : 720
Image Height                    : 480
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 720x480
Megapixels                      : 0.346
Thumbnail Image                 : (Binary data 25217 bytes, use -b option to extract)

$ exiftool -b -ThumbnailImage leap_of_faith.jpeg > thumb.jpg
```

![](https://i.imgur.com/Dmb0Yj2.jpg)

```
$ exiftool thumb.jpg 
ExifTool Version Number         : 11.30
File Name                       : thumb.jpg
Directory                       : .
File Size                       : 25 kB
File Modification Date/Time     : 2019:04:07 00:28:12+02:00
File Access Date/Time           : 2019:04:07 00:28:12+02:00
File Inode Change Date/Time     : 2019:04:07 00:28:12+02:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Little-endian (Intel, II)
X Resolution                    : 72
Y Resolution                    : 72
Resolution Unit                 : inches
Modify Date                     : 2019:04:04 09:23:36
Exif Version                    : 0210
Date/Time Original              : 2019:04:04 09:23:36
Flashpix Version                : 0100
Color Space                     : Uncalibrated
Thumbnail Offset                : 226
Thumbnail Length                : 5199
Image Width                     : 334
Image Height                    : 302
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 334x302
Megapixels                      : 0.101
Thumbnail Image                 : (Binary data 5199 bytes, use -b option to extract)

$ exiftool -b -ThumbnailImage thumb.jpg > thumb_thumb.jpg
```
![](https://i.imgur.com/0RddQWn.jpg)

`flag{FR33_Y0UR_M1ND}`

Note: stego is not forensics

## 50 - Last Transmission - Stego

> Two of our squad leaders got beamed up before they could finish their transmission, but they left a clue behind as to where they were taken.
>
> -= Created by P4PA_0V3RL0RD =-

Original image:

![](https://i.imgur.com/r5GbxE8.jpg)

Blue 0 plane:

![](https://i.imgur.com/776ILUq.png)

Red 1 plane:

![](https://i.imgur.com/GYLxt4C.png)

`flag{B34M_M3_UP_SC077Y}`

---
layout: post
title: "The rather unusual, opinionated and commented ArchLinux installation"
date: 2019/01/16
updated: 2019/05/11
lang: en
categories:
- linux
- archlinux
tags:
- linux
- archlinux
thumbnail: /images/archlinux.svg
authorId: noraj
toc: true
---
First of all, this tutorial doesn't prevent you from following the [ArchWiki - Installation guide][archwiki_install_guide], it is not standalone.

## Pre-installation

### First basic steps

For those first steps, I think you are a big boy enough to do them alone.

So you can download the ArchLinux iso, verify its signature, boot the live environment, set the keyboard layout, verify the boot mode, connect to the internet, update the system clock.
If you're not confident with those steps check the [ArchWiki][archwiki_install_guide].

### Partition the disks

Identify the block device associated to disks with `lsblk` or `fdisk -l`.

Now we will use [dm-crypt][archwiki_dmcrypt] to [encrypt an entire system][archwiki_encrypting_entire_system] with [LVM on LUKS][archwiki_lvm_on_luks] on only one disk.

[UEFI][archwiki_uefi] is enabled, so I will use a GPT partition type and an [EFI system partition][archwiki_esp] (ESP).

So we will have two partitions: one ESP and one partition that will host the LUKS container.

```
# fdisk /dev/sda
g # create GUID Partition Table (GPT)
n # create a new partition (EFI system partition)
1 # partition number
2048 # 1st sector
+550M # last sector
t # change partition type
1 # EFI system
n # create a new partition (LVM for later LUKS encrypted container)
2 # partition number
<ENTER> # default 1st sector
<ENTER> # default last sector
t # change partition type
2 # select partiton 2
31 # partition type: Linux LVM
p # print the partition table
w # write table to disk and exit
```

### Secure erase

Don't forget to check the [drive preparation][archwiki_drive_preparation].

### LUKS container

Lot of people will use the default values of `cryptsetup` but for a more secure setup I used `camellia` for ciphering rather than the NIST validated (understand NSA compliant) AES algorithm, the much stronger and newer password-based key derivation function `argon2` rather than the default `pbkdf2`, and the SHA-2 `sha512` instead of the default `sha256` because SHA-3 `keccak` or finalist `blake2` are not available here.

`cryptsetup becnhmark` won't show you those and sometimes even `/proc/crypto` will not show you `camellia` for example (even if it is available).

Create the LUKS encrypted container:

```
# cryptsetup luksFormat --type luks2 --cipher camellia-xts-plain64 --key-size 512 --iter-time 2000 --pbkdf argon2id --hash sha512 /dev/sda2
```

Open the LUKS container:

```
# cryptsetup open /dev/sda2 cryptlvm
```

The decrypted container is now available at `/dev/mapper/cryptlvm`.

### Preparing the logical volumes

Create a physical volume on top of the opened LUKS container:

```
# pvcreate /dev/mapper/cryptlvm
```

Create a volume group, adding the previously created physical volume to it:

```
# vgcreate myvg /dev/mapper/cryptlvm
```

Create all your logical volumes on the volume group:

```
# lvcreate -L 8G myvg -n swap
# lvcreate -l 100%FREE myvg -n root
```

Format your filesystems on each logical volume:

```
# mkfs.fat -F32 /dev/sda1
# mkfs.ext4 /dev/myvg/root # or /dev/mapper/myvg-root
# mkswap /dev/myvg/swap # or /dev/mapper/myvg-swap
```

Mount your filesystems:

```
# mount /dev/myvg/root /mnt
# swapon /dev/myvg/swap
# mkdir /mnt/boot/ && mkdir /mnt/efi
# mount /dev/sda1 /mnt/efi # mount ESP to /efi outside /boot
```

Check the partition table: `lsblk -f /dev/sda`.

## Installation

### Select the mirrors

Again, here it let you [select the mirrors][archwiki_select_mirrors].

### Install the base packages

Install the base + some useful packages:

```
# pacstrap /mnt base linux-lts base-devel sudo wget curl lvm2
```

### Fstab

Generate an fstab file by UUID:

```
# genfstab -U /mnt >> /mnt/etc/fstab
```

Check `/mnt/etc/fstab` correctness and add `/efi/EFI/arch /boot none defaults,bind 0 0` to mount the EFI mountpoint at boot since we mounted ESP outside of `/boot`.

So you should have something similar to:

```
# Static information about the filesystems.
# See fstab(5) for details.

# <file system> <dir> <type> <options> <dump> <pass>

# /dev/mapper/myvg-root
UUID=b1566d2d-96db-4efb-8098-06cbdc2ba17d	/         	ext4      	rw,relatime	0 1

# /dev/sda1
UUID=3203-97C0      	/efi      	vfat      	rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro	0 2

# /dev/mapper/myvg-swap
UUID=e0729c70-df94-44c9-849a-1f1cfedc5db8	none      	swap      	defaults,pri=-2	0 0

/efi/EFI/arch	/boot	none	defaults,bind	0 0
```

### Chroot

Change root into the new system:

```
# arch-chroot /mnt
```

### Time zone

Set the time zone:

```
# ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
```

Run `hwclock` to generate `/etc/adjtime`:

```
# hwclock --systohc
```

### Localization

Uncomment locales in `/etc/locale.gen`, and generate them with:

```
# locale-gen
```

As I'm French, for me locales were:

```
en_US.UTF-8 UTF-8
fr_FR.UTF-8 UTF-8
```

Set variables in `/etc/locale.conf`, for example:

```
LC_ADDRESS=fr_FR.UTF-8
LC_COLLATE=fr_FR.UTF-8
LC_CTYPE=fr_FR.UTF-8
LC_IDENTIFICATION=fr_FR.UTF-8
LC_MONETARY=fr_FR.UTF-8
LC_MESSAGES=en_US.UTF-8
LC_MEASUREMENT=fr_FR.UTF-8
LC_NAME=fr_FR.UTF-8
LC_NUMERIC=fr_FR.UTF-8
LC_PAPER=fr_FR.UTF-8
LC_TELEPHONE=fr_FR.UTF-8
LC_TIME=fr_FR.UTF-8
LANG=en_US.UTF-8
LANGUAGE=en_US:en
```

Because I want all sort of format to be displayed like we do in France but keep the system and displayed messages in English.

Set the keyboard layout in `/etc/vconsole.conf`, for example (for `AZERTY` default keyboard):

```
KEYMAP=fr
```

### Network configuration

Create the hostname file (`/etc/hostname`):

```
archway
```

Add matching entries to `/etc/hosts`:

```
127.0.0.1 localhost
::1       localhost
```

### Initramfs

Configuring mkinitcpio HOOKS in `/etc/mkinitcpio.conf` to work with `encrypt`:

```
HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt lvm2 resume filesystems fsck)
```

Recreate the initramfs image:

```
# mkinitcpio -p linux
```

Copy linux image to ESP:

```
# mkdir -p /efi/EFI/arch
# cp -a /boot/vmlinuz-linux /efi/EFI/arch/
# cp -a /boot/initramfs-linux.img /efi/EFI/arch/
# cp -a /boot/initramfs-linux-fallback.img /efi/EFI/arch/
```

**Warning**: see [Mount the partition](https://wiki.archlinux.org/index.php/EFI_system_partition#Mount_the_partition), `/efi` is a standard mount point so there is not need for a bind mount that is only required for alternative mount points. So you can also skip those last steps and remove the additional line in `/etc/fstab`.

### Root password

Easy!

Change root password:

```
# passwd
```

### Boot loader + Microcode

I know what you're about to say:

> WTF man! Why don't you use GRUB?

Because rEFInd works better for EFI partitions as the name states.

```
# pacman -S refind-efi intel-ucode
# refind-install
```

**Warning**: this won't work for VirtualBox, check the ArchWiki.

Then we need to edit `/boot/refind_linux.conf`:

```
"Boot with default options"  "cryptdevice=UUID=fcaa743b-9ad8-4699-9329-fbb9bec4de80:cryptlvm root=/dev/myvg/root rw add_efi_memmap initrd=/EFI/arch/intel-ucode.img initrd=/EFI/arch/initramfs-%v.img resume=/dev/myvg/swap"
"Boot with fallback initramfs"    "cryptdevice=UUID=fcaa743b-9ad8-4699-9329-fbb9bec4de80:cryptlvm root=/dev/myvg/root rw add_efi_memmap initrd=/EFI/arch/intel-ucode.img initrd=/EFI/arch/initramfs-%v-fallback.img resume=/dev/myvg/swap"
"Boot to terminal"   "cryptdevice=UUID=fcaa743b-9ad8-4699-9329-fbb9bec4de80:cryptlvm root=/dev/myvg/root rw add_efi_memmap systemd.unit=multi-user.target resume=/dev/myvg/swap"
```

Copy `/boot/refind_linux.conf` to `/efi/EFI/arch/refind_linux.conf`.

And also edit `/efi/EFI/refind/refind.conf` in order to work with `%v` in `refind_linux.conf`:

```
...
extra_kernel_version_strings linux-lts,linux
...
```

So this way we have to configure the boot entries only once for multiple kernels.

Do not bind mount the ESP to `/boot` before using `refind-install` else it will fail:

```
# mount --bind /efi/EFI/arch /boot
```

#### Alternative bootloader

If you like pain (and don't have any style), you can still try to make GRUB2 works with an UEFI LVM on LUKS install.

```
$ sudo pacman -S grub ntfs-3g os-prober
```

Since we have both `linux` and `linux-lts` installed we want a better [multiple kernels management](https://wiki.archlinux.org/index.php/GRUB/Tips_and_tricks#Multiple_entries):

So we will edit `/etc/default/grub`:

- Disable submenu: `GRUB_DISABLE_SUBMENU=y`
- Recall previous entry: `GRUB_DEFAULT=saved` and `GRUB_SAVEDEFAULT=true`

No we need the configuration for LVM on LUKS:

```
GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR="Arch"
GRUB_CMDLINE_LINUX="cryptdevice=UUID=290e01ae-b0cb-4843-8533-fe5a448299f9:cryptlvm"
GRUB_CMDLINE_LINUX_DEFAULT="resume=/dev/myvg/swap"
```

Add some custom entries in `/etc/grub.d/40_custom`:

```sh
#!/bin/sh
exec tail -n +3 $0
# This file provides an easy way to add custom menu entries.  Simply type the
# menu entries you want to add after this comment.  Be careful not to change
# the 'exec tail' line above.
menuentry "System shutdown" {
  echo "System shutting down"
  halt
}

menuentry "System restart" {
  echo "System rebooting"
  reboot
}

if [ ${grub_platform} == "efi" ]; then
  menuentry "Firmware setup" {
    fwsetup
  }
fi
```

Install grub fully into the ESP:

```
# grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=grub --boot-directory=/efi
```

Then generate the grub config:

```
# grub-mkconfig -o /efi/grub/grub.cfg
```

The linux part of `/efi/grub/grub.cfg` should now looks like this:


```sh
### BEGIN /etc/grub.d/10_linux ###
menuentry 'Arch Linux, with Linux linux-lts' --class arch --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-linux-lts-advanced-6d84cc5c-6109-4c21-98c4-c5ec95f31ba4' {
        savedefault
        load_video
        set gfxpayload=keep
        insmod gzio
        insmod part_gpt
        insmod fat
        set root='hd0,gpt1'
        if [ x$feature_platform_search_hint = xy ]; then
          search --no-floppy --fs-uuid --set=root --hint-ieee1275='ieee1275//disk@0,gpt1' --hint-bios=hd0,gpt1 --hint-efi=hd0,gpt1 --hint-baremetal=ahci0,gpt1  2EE2-5ECE
        else
          search --no-floppy --fs-uuid --set=root 2EE2-5ECE
        fi
        echo    'Loading Linux linux-lts ...'
        linux   /EFI/arch/vmlinuz-linux-lts root=/dev/mapper/myvg-root rw cryptdevice=UUID=290e01ae-b0cb-4843-8533-fe5a448299f9:cryptlvm resume=/dev/myvg/swap
        echo    'Loading initial ramdisk ...'
        initrd  /EFI/arch/intel-ucode.img /EFI/arch/initramfs-linux-lts.img
}
menuentry 'Arch Linux, with Linux linux-lts (fallback initramfs)' --class arch --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-linux-lts-fallback-6d84cc5c-6109-4c21-98c4-c5ec95f31ba4' {
        savedefault
        load_video
        set gfxpayload=keep
        insmod gzio
        insmod part_gpt
        insmod fat
        set root='hd0,gpt1'
        if [ x$feature_platform_search_hint = xy ]; then
          search --no-floppy --fs-uuid --set=root --hint-ieee1275='ieee1275//disk@0,gpt1' --hint-bios=hd0,gpt1 --hint-efi=hd0,gpt1 --hint-baremetal=ahci0,gpt1  2EE2-5ECE
        else
          search --no-floppy --fs-uuid --set=root 2EE2-5ECE
        fi
        echo    'Loading Linux linux-lts ...'
        linux   /EFI/arch/vmlinuz-linux-lts root=/dev/mapper/myvg-root rw cryptdevice=UUID=290e01ae-b0cb-4843-8533-fe5a448299f9:cryptlvm resume=/dev/myvg/swap
        echo    'Loading initial ramdisk ...'
        initrd  /EFI/arch/initramfs-linux-lts-fallback.img
}
menuentry 'Arch Linux, with Linux linux' --class arch --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-linux-advanced-6d84cc5c-6109-4c21-98c4-c5ec95f31ba4' {
        savedefault
        load_video
        set gfxpayload=keep
        insmod gzio
        insmod part_gpt
        insmod fat
        set root='hd0,gpt1'
        if [ x$feature_platform_search_hint = xy ]; then
          search --no-floppy --fs-uuid --set=root --hint-ieee1275='ieee1275//disk@0,gpt1' --hint-bios=hd0,gpt1 --hint-efi=hd0,gpt1 --hint-baremetal=ahci0,gpt1  2EE2-5ECE
        else
          search --no-floppy --fs-uuid --set=root 2EE2-5ECE
        fi
        echo    'Loading Linux linux ...'
        linux   /EFI/arch/vmlinuz-linux root=/dev/mapper/myvg-root rw cryptdevice=UUID=290e01ae-b0cb-4843-8533-fe5a448299f9:cryptlvm resume=/dev/myvg/swap
        echo    'Loading initial ramdisk ...'
        initrd  /EFI/arch/intel-ucode.img /EFI/arch/initramfs-linux.img
}
menuentry 'Arch Linux, with Linux linux (fallback initramfs)' --class arch --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-linux-fallback-6d84cc5c-6109-4c21-98c4-c5ec95f31ba4' {
        savedefault
        load_video
        set gfxpayload=keep
        insmod gzio
        insmod part_gpt
        insmod fat
        set root='hd0,gpt1'
        if [ x$feature_platform_search_hint = xy ]; then
          search --no-floppy --fs-uuid --set=root --hint-ieee1275='ieee1275//disk@0,gpt1' --hint-bios=hd0,gpt1 --hint-efi=hd0,gpt1 --hint-baremetal=ahci0,gpt1  2EE2-5ECE
        else
          search --no-floppy --fs-uuid --set=root 2EE2-5ECE
        fi
        echo    'Loading Linux linux ...'
        linux   /EFI/arch/vmlinuz-linux root=/dev/mapper/myvg-root rw cryptdevice=UUID=290e01ae-b0cb-4843-8533-fe5a448299f9:cryptlvm resume=/dev/myvg/swap
        echo    'Loading initial ramdisk ...'
        initrd  /EFI/arch/initramfs-linux-fallback.img
}

### END /etc/grub.d/10_linux ###
```

## Reboot

You know how to reboot right?

Ok ok, but it's better to unmount all the partitions first `umount -R /mnt`.

## Post-installation

### Before we begin

It could be nice to setup a DHCP client to avoid manual IP configuration.

Enable DHCP client:

```
# systemctl start dhcpcd
```

Now we have Internet access, let's update the system before installing anything:

```
# pacman -Syu
```

We'll use a lot this terminal so let's get a fancier zsh shell:

```
# pacman -S zsh zsh-autosuggestions zsh-completions zsh-history-substring-search zsh-syntax-highlighting
```

### System administration

#### Users, groups and privilege escalation

We already installed `sudo` with `pacstrap`.

Add a new user and assign sudo privilege

```
# useradd -m -G wheel -s /bin/zsh noraj
# passwd noraj
# visudo
```

And uncomment `%wheel ALL=(ALL) ALL`.

Exit root session and log back as user.

Creating default XDG directories

```
$ sudo pacman -S xdg-user-dirs
$ xdg-user-dirs-update
```

### Package management

#### Repositories

Send stats about packages

```
$ sudo pacman -S pkgstats
```

#### Arch User Repository

Install a [pacman wrapper][archwiki_acman_wrappers] for AUR support, for example `pikaur`, `pakku`, `yay`:

```
$ sudo pacman -S git
$ cd /tmp
$ git clone https://aur.archlinux.org/pikaur.git
$ cd pikaur
$ makepkg -si
```

Please, don't install `yaourt`, check the [pacman wrapper][archwiki_pacman_wrappers] ArchWiki page.

### Graphical user interface

#### Display server and display drivers

Install the display server, some utils and associated drivers

```
$ sudo pacman -S xorg-server xorg-xrandr
$ sudo pacman -S xf86-video-intel xf86-video-nouveau mesa mesa-demos
```

#### Desktop environments

As we want a **true** graphical library backed desktop environment (understand a Qt DE as GTK is only the GIMP library), we have barely two choices: KDE or LXQT, but LXQT is very light (nice for a VM but too light for a nice desktop experience).

Install KDE Desktop Environment

```
$ sudo pacman -S plasma-meta
$ sudo systemctl enable sddm
```

Configure KDE:

- System Settings > Desktop Behavior > Desktop Effects > Disable `Translucency` that behave bad for dark themes.
- System Settings > Startup and Shutdown > Background Services > Disable Bluetooth, we don't need it
- System Settings > Search > File Search > Deselect "Enable File Search"
- System Settings > Regional Settings > Set Language and Formats
- System Settings > Inputs Devices > Keyboard > Layouts > Check `Configure layouts` and add your keymap

PS: you may want to install a VTE before rebooting or you'll be forced to use a TTY.

### Networking

If not already installed, install NetworkManager network manager and applets:

```
$ sudo pacman -S networkmanager kdeplasma-addons plasma-nm
$ sudo systemctl enable NetworkManager
$ sudo systemctl start NetworkManager
$ sudo systemctl disable netctl
```

Strenght of `NetworkManager` are: official package for KDE applet, integrated wifi manager, nice integration with KDE.

Drawback of `NetworkManger`: does not support the use of `dhcpcd` for IPv6 currently. So let's change of DHCP client and use `dhclient` instead.

```
$ sudo pacman -S dhclient # not running as systemd service unlike dhcpcd
$ sudo systemctl disable dhcpcd
$ sudo systemctl stop dhcpcd
$ sudoedit /etc/NetworkManager/conf.d/dhcp-client.conf
[main]
dhcp=dhclient
$ sudo systemctl restart NetworkManager
```

Encrypted Wi-Fi passwords by [using KDE wallet][archwiki_using_kde_wallet].

Disallow `/etc/resolv.conf` overwrite:

```
$ sudoedit /etc/NetworkManager/conf.d/dns.conf
[main]
dns=none
```

### General

#### Software

Install a VTE (Virtual Terminal Emulator):

```
$ sudo pacman -S konsole qterminal
```

Install net browsers and plugins, Firefox is far more powerful but use GTK where Falkon is using Qt but is far to be complete and fast. But anyway having several browser is always useful.

```
$ sudo pacman -S firefox falkon
$ sudo pacman -S arch-firefox-search firefox-dark-reader firefox-extension-https-everywhere firefox-extension-privacybadger firefox-stylus firefox-ublock-origin firefox-umatrix
```

Install media software (lot of codecs are already installed as dependencies of media players):

- video player: vlc, smaplayer
- media metadata: mediainfo mediainfo-gui
- video converter: handbrake
- download youtube audio/video: youtube-dl
- audio player: audacious clementine elisa
- image viewer: nomacs gwenview

```
$ sudo pacman -S vlc smplayer mediainfo mediainfo-gui handbrake youtube-dl audacious clementine elisa nomacs gwenview
```

Install general software:

```
$ sudo pacman -S keepassxc kmail code kate okular qbittorrent quassel-monolithic speedcrunch dolphin xsel p7zip unrar aria2 bleachbit openssh expect ksysguard htop nfoview
```

#### Fonts

Install some fonts!

```
$ sudo pacman -S ttf-liberation noto-fonts ttf-roboto ttf-anonymous-pro ttf-hack ttf-inconsolata noto-fonts-emoji powerline-fonts adobe-source-code-pro-fonts ttf-fira-mono
```

#### Theming

##### Colorized command output

Aliases for colorized output:

```
alias diff='diff --color=auto'
alias grep='grep --color=auto'
alias ls='ls --color=auto'
export LESS=-R
man() {
  LESS_TERMCAP_md=$'\e[01;31m' \
  LESS_TERMCAP_me=$'\e[0m' \
  LESS_TERMCAP_se=$'\e[0m' \
  LESS_TERMCAP_so=$'\e[01;44;33m' \
  LESS_TERMCAP_ue=$'\e[0m' \
  LESS_TERMCAP_us=$'\e[01;32m' \
  command man "$@"
}
```

Color wrappers:

```
$ sudo pacman -S grc
```

##### KDE

KDE icon Theme

```
$ sudo pacman -S papirus-icon-theme
```

I personnaly installed the [Midnight](https://store.kde.org/p/1231509/) theme. Go in `System Settings` to install and apply it.

Then the kvantum associated theme:

```
$ sudo pacman -S kvantum-qt5
$ wget -qO- https://raw.githubusercontent.com/Rokin05/midnight-kde/master/install.sh | sh
```

Then apply the vantum theme in `kvantummanager`.

##### Oh-my-zsh

Install oh-my-zsh:

```
$ pikaur -S oh-my-zsh-git
```

Then I'm using the [Spaceship](https://github.com/denysdovhan/spaceship-prompt) ZSH theme:

```
$ pikaur -S spaceship-prompt-git
```

And since we are using the AUR package of oh-my-zsh, we will use the spaceship theme as an oh-my-zsh theme:

```
$ sudo ln -s /usr/lib/spaceship-prompt/spaceship.zsh-theme /usr/share/oh-my-zsh/custom/themes/spaceship.zsh-theme
```

You may need to take a look at issue [#661](https://github.com/denysdovhan/spaceship-prompt/issues/661).

I'm using [this zshrc](https://gitlab.com/noraj/my-config-files/blob/master/.zshrc).

##### Tmux

Install a Terminal multiplexers:

```
$ sudo pacman -S tmux
```

Then I'm using a Powerline theme of [Tmux Themepack](https://github.com/jimeh/tmux-themepack).

This manually installable like that:

```
$ git clone https://github.com/jimeh/tmux-themepack.git ~/.tmux-themepack
```

Then adding a line with the desired theme in `~/.tmux.conf`:

```
source-file "${HOME}/.tmux-themepack/powerline/default/red.tmuxtheme"
```

I'm using [this tmux conf](https://gitlab.com/noraj/my-config-files/blob/master/.tmux.conf).

##### Neovim

Install [neovim][neovim] and [vim-plug][vim-plug] (neovim plugin manager):

```
$ sudo pacman -S neovim
$ curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Then launch neovim by using the `nvim` command and install the plugins you may have in your neovim config file by using `:PlugInstall`.

I'm using [this neovim config](https://gitlab.com/noraj/my-config-files/blob/master/.vimrc2) but I was used to use [this alternative one](https://gitlab.com/noraj/my-config-files/blob/master/.vimrc) before.

##### SDDM

Install SDDM theme ([sddm-sugar-dark][sddm-sugar-dark]) via *System settings* > *Startup and Shutdown* > *Login Screen (SDDM)* > *Theme* > *Get new login screens*.
Then custom the theme: `sudoedit /usr/share/sddm/themes/sugar-dark/theme.conf`.
Most often you'll want to change those parts:

```ini
[General]
Background="Background.jpg"
; Must match the name of the image in the theme directory. Any standard image file format is allowed with transparency supported. (e.g. background.jpeg/illustration.GIF/Foto.png)
ScreenWidth=1920
ScreenHeight=1080
; Adjust to your resolution to help SDDM speed up on calculations

# [Design Customizations]
ThemeColor="snow"
AccentColor="dodgerblue"
```

#### Nice to have

Set X11 keyboard layout (example: for password prompt in SDDM):

```
$ setxkbmap -layout fr
$ sudo localectl set-x11-keymap fr
```

Get [kalu](https://jjacky.com/kalu/) for archlinux news and updates notifications:

```
$ pikaur -S kalu-kde
```

PS: add kalu to autostart applications

For automounting device:

```
$ sudo pacman -S udisks2 udiskie
```

[Flameshot](https://github.com/lupoDharkael/flameshot) is a great screenshot application.

```
$ sudo pacman -S flameshot
$ mkdir -p ~/Pictures/Screenshots
```

Then follow [those steps](https://github.com/lupoDharkael/flameshot#on-kde-plasma-desktop) for adding shortcuts.

Spell checker (this one will work with vscode):

```
$ sudo pacman -S hunspell hunspell-en_US hunspell-fr
```

### Reflector

Install [Reflector][reflector]:

> a script which can retrieve the latest mirror list from the MirrorStatus page, filter the most up-to-date mirrors, sort them by speed

```
$ sudo pacman -S reflector
```

[reflector]:https://wiki.archlinux.org/index.php/Reflector

#### Automation

##### Pacman hook

You can also create a pacman hook that will run reflector and remove the `.pacnew` file created every time `pacman-mirrorlist` gets an upgrade.

Create `/etc/pacman.d/hooks/mirrorupgrade.hook`:

```
$ sudo mkdir /etc/pacman.d/hooks/
$ sudoedit /etc/pacman.d/hooks/mirrorupgrade.hook
```

```ini
[Trigger]
Operation = Upgrade
Type = Package
Target = pacman-mirrorlist

[Action]
Description = Updating pacman-mirrorlist with reflector and removing pacnew...
When = PostTransaction
Depends = reflector
Exec = /bin/sh -c "reflector --country France --age 24 --sort rate --save /etc/pacman.d/mirrorlist; rm -f /etc/pacman.d/mirrorlist.pacnew"
```

This will get an unlimited list of all type of mirrors (IPv4/IPv6, ftp,https,http,rsync) located in France that synchronized within the last 24 hours and sort them by download speed.

##### Systemd service timer

Run `reflector` on a weekly basis, create `/etc/systemd/system/reflector.timer`:

```ini
[Unit]
Description=Run reflector weekly

[Timer]
OnCalendar=Mon *-*-* 7:00:00
RandomizedDelaySec=15h
Persistent=true

[Install]
WantedBy=timers.target
```

But we will also need a service file `/etc/systemd/system/reflector.service`:

```ini
[Unit]
Description=Pacman mirrorlist update
Wants=network-online.target
After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/reflector --country France --age 24 --sort rate --save /etc/pacman.d/mirrorlist

[Install]
RequiredBy=multi-user.target
```

You can then start reflector one shot with `systemctl start reflector.service`, or enable it to start at each boot with `systemctl enable reflector.service` or just use the one week timer: `systemctl start reflector.timer` and `systemctl enable reflector.timer`.

PS: do not forget `sudo systemctl daemon-reload` to get thw ner service available.

### Virtualbox (host)

Install [VirtualBox][virtualbox], the dkms module and linux hearders:

```
$ sudo pacman -S virtualbox virtualbox-host-dkms linux-headers linux-lts-headers
```

Install the extension pack:

```
$ pikaur -S virtualbox-ext-oracle
```

[archwiki_install_guide]:https://wiki.archlinux.org/index.php/installation_guide
[archwiki_dmcrypt]:https://wiki.archlinux.org/index.php/Dm-crypt 
[archwiki_encrypting_entire_system]:https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system
[archwiki_lvm_on_luks]:https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS
[archwiki_uefi]:https://wiki.archlinux.org/index.php/UEFI
[archwiki_esp]:https://wiki.archlinux.org/index.php/EFI_system_partition
[archwiki_drive_preparation]:https://wiki.archlinux.org/index.php/Dm-crypt/Drive_preparation
[archwiki_select_mirrors]:https://wiki.archlinux.org/index.php/installation_guide#Select_the_mirrors
[archwiki_pacman_wrappers]:https://wiki.archlinux.org/index.php/AUR_helpers#Pacman_wrappers
[archwiki_using_kde_wallet]:https://wiki.archlinux.org/index.php/NetworkManager#Using_KDE_Wallet
[neovim]:https://neovim.io/
[vim-plug]:https://github.com/junegunn/vim-plug
[sddm-sugar-dark]:https://github.com/MarianArlt/sddm-sugar-dark
[virtualbox]:https://wiki.archlinux.org/index.php/VirtualBox

# RawSec

Static website hosted on GitLab Pages

**Topics & Tags** : Security (Cybersecurity), Linux, News, System, Virtualization, ...

![rawsec.ml](http://i.imgur.com/hQtyEaz.png)

## Dev

Install the required version of nodejs with nodenv.

Then install the hexo cli tool:

```
$ npm install -g hexo-cli
```

Then install dependencies:

```
$ npm i
```

Then build and serve:

```
$ hexo g && hexo s
```
---
layout: post
title: "CBM CTF 2019 - Write-ups"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - web
date: 2019/04/07
thumbnail: /images/ctf.png
authorId: noraj
toc: true
---
## Information

### CTF

- **Name** : CBM CTF 2019
- **Website** : [cbmctf2019.tk](http://cbmctf2019.tk/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/783)

## 45 - Long road - Web

> Description: website is at http://cbmctf2019.cf:5000

```
$ curl http://cbmctf2019.cf:5000/
<form action="/cool"><input type="submit" value="cool" /></form>
```

`/cool` is a HTTP 302 redirection to `/cool1` that is a HTTP 302 redirection to `/cool2` etc. until `/destination` is reached.

Let's log all the traffic through Burp and see if there is something interesting in between the start and the end.

![](https://i.imgur.com/ViJnoFq.png)

The flag is in the `Set-Cookie` HTTP header of the `/cool8` response.

`cbmctf{tracking_redirects!!}`

## 55 - In mountains I feel fresh - Web

> Description: website is at http://cbmctf2019.cf:5001

```
$ curl -v http://cbmctf2019.cf:5001/
*   Trying 35.231.181.111...
* TCP_NODELAY set
* Connected to cbmctf2019.cf (35.231.181.111) port 5001 (#0)
> GET / HTTP/1.1
> Host: cbmctf2019.cf:5001
> User-Agent: curl/7.64.1
> Accept: */*
> 
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Content-Type: text/html; charset=utf-8
< Content-Length: 476
< Vary: Cookie
< Set-Cookie: session=eyJ2aXNpdHMiOjF9.XKk6Hg.KhrS6NOVPIU650njthC8IFdzoWM; HttpOnly; Path=/
< Server: Werkzeug/0.15.1 Python/2.7.13
< Date: Sat, 06 Apr 2019 23:45:34 GMT
< 
* Closing connection 0
Narcissists will never tell you the truth. They live with the fear of abandonment and can't deal with facing their own shame. Therefore, they will twist the truth, downplay their behavior, blame others and say what ever it takes to remain the victim. They are master manipulators and conartists that don't believe you are smart enough to figure out the depth of their disloyalty. Their needs will always be more important than telling you any truth that isn't in their favor..
```

This is a JWT token `eyJ2aXNpdHMiOjF9.XKk6Hg.KhrS6NOVPIU650njthC8IFdzoWM` it contains `{"visits": 1}`.

We can't spoof that token so I guess we must set the cookie and make another request so server will ask us to set `{"visits": 2}` etc. and a lot of request later we will get the flag.

I could have write a ruby script but I was curious if Burp Suite could do it.

So here what I did:

1. Let's send the request to the **Intruder**
2. Add a **Payload position** at the session cookie value ![](https://i.imgur.com/ipdgFhF.png)
3. Go to the **Payloads** tab and in the **Payloads Sets** section you must set **Payload type** as **Recursive grep** ![](https://i.imgur.com/esBHX0W.png), this will extract the cookie from the `Set-Cookie` server response and set it for the next request.
4. Go to the **Options** tab, then go to the **Grep - Extract** section, check the option *Extract the following items from responses* ![](https://i.imgur.com/7zaDSs9.png), click **Add** and select the cookie value ![](https://i.imgur.com/b6hptDg.png): this will automatically set an extract rule
5. Then go to the **Grep - Match** section, check the option *Flag result items with responses matching these expressions*, add `cbmctf` and `flag` in order to match the flag when it will appear, uncheck the option *Exclude HTTP headers* in case the flag is in a cookie or something similar ![](https://i.imgur.com/ovxbQXD.png)
6. Go back to he **Payloads** tab, then go to the **Payload Options [Recursive grep]** section, select the extract rule we just set, add the initial payload `eyJ2aXNpdHMiOjR9.eXKk7JA.ei8tMzkQyg7An4GW3KciwB61LEUk` and be don't check the option *Stop if duplicate payload found* ![](https://i.imgur.com/gNaFnwY.png)
7. Go to the **Payload Processing** section and add a **URL-decode** rule else the JWT token library won't be able to understand that `%2e` is a dot `.` and you will repeat visit n°1 again and again ![](https://i.imgur.com/juPZu8y.png)
8. Go to **Payload Encoding** section and uncheck *URL-encode these characters*, else dot will be re-encoded right after being decoded ![](https://i.imgur.com/ZS5TvYr.png)
9. Click **Start attack**
10. Wait, we didn't made the match rules for nothing, we don't have to manually check each answer ![](https://i.imgur.com/ANS8uLl.png)

I have to admit it was a smart idea.

The flag was `cbmctf{s335!on_c00k!35}`.

## 80 - What is Your Name? - Web

> Description: site is at http://cbmctf2019.cf:2002/ [NOTE]: Its not always about finding vulnerability.

```
$ curl http://cbmctf2019.cf:2002/welcome?name=flag
Worst name I ever heard. Please change it.
```

The hint says it is not about a vulnerability so it is no use to try SQLi, LDAPi or whatever.

I tried some enumeration or bruteforcing some usernames but an admin told it was not about bruteforce.

It was about guessing, at some point I tried some HTTP verbs/methods:

```
$ curl -v "http://cbmctf2019.cf:2002/" -X OPTIONS 
*   Trying 35.231.181.111...
* TCP_NODELAY set
* Connected to cbmctf2019.cf (35.231.181.111) port 2002 (#0)
> OPTIONS / HTTP/1.1
> Host: cbmctf2019.cf:2002
> User-Agent: curl/7.64.1
> Accept: */*
> 
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Content-Type: text/html; charset=utf-8
< Allow: HEAD, OPTIONS, GET
< Content-Length: 0
< Server: Werkzeug/0.15.1 Python/2.7.13
< Date: Sun, 07 Apr 2019 17:05:14 GMT
```

But there was only `HEAD, OPTIONS, GET` allowed.

Then I tried an arbitrary method:

```
$ curl -v "http://cbmctf2019.cf:2002/" -X FLAG   
*   Trying 35.231.181.111...
* TCP_NODELAY set
* Connected to cbmctf2019.cf (35.231.181.111) port 2002 (#0)
> FLAG / HTTP/1.1
> Host: cbmctf2019.cf:2002
> User-Agent: curl/7.64.1
> Accept: */*
> 
* HTTP 1.0, assume close after body
< HTTP/1.0 405 METHOD NOT ALLOWED
< Content-Type: text/html
< Allow: HEAD, OPTIONS, GET
< Content-Length: 178
< Server: Werkzeug/0.15.1 Python/2.7.13
< Date: Sun, 07 Apr 2019 17:12:42 GMT
< 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>405 Method Not Allowed</title>
<h1>Method Not Allowed</h1>
<p>The method is not allowed for the requested URL.</p>
```

Nothing more. A gave up that idea before I tried again but on `/welcome`.

```
$ curl -v "http://cbmctf2019.cf:2002/welcome" -X OPTIONS
*   Trying 35.231.181.111...
* TCP_NODELAY set
* Connected to cbmctf2019.cf (35.231.181.111) port 2002 (#0)
> OPTIONS /welcome HTTP/1.1
> Host: cbmctf2019.cf:2002
> User-Agent: curl/7.64.1
> Accept: */*
> 
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Content-Type: text/html; charset=utf-8
< Content-Length: 19
< Server: Werkzeug/0.15.1 Python/2.7.13
< Date: Sun, 07 Apr 2019 17:14:24 GMT
< 
* Closing connection 0
Don't be over smart
```

We have a message: `Don't be over smart`. Let's try another HTTP method on that endpoint:

```
$ curl -v "http://cbmctf2019.cf:2002/welcome" -X FLAG
*   Trying 35.231.181.111...
* TCP_NODELAY set
* Connected to cbmctf2019.cf (35.231.181.111) port 2002 (#0)
> FLAGS /welcome HTTP/1.1
> Host: cbmctf2019.cf:2002
> User-Agent: curl/7.64.1
> Accept: */*
> 
* HTTP 1.0, assume close after body
< HTTP/1.0 405 METHOD NOT ALLOWED
< Content-Type: text/html
< Allow: HEAD, GET, PUT, POST, DELETE, OPTIONS, PATCH
< Content-Length: 178
< Server: Werkzeug/0.15.1 Python/2.7.13
< Date: Sun, 07 Apr 2019 17:48:18 GMT
< 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>405 Method Not Allowed</title>
<h1>Method Not Allowed</h1>
<p>The method is not allowed for the requested URL.</p>
```

Let's try one of those methods:

```
$ curl -v "http://cbmctf2019.cf:2002/welcome" -X PATCH
*   Trying 35.231.181.111...
* TCP_NODELAY set
* Connected to cbmctf2019.cf (35.231.181.111) port 2002 (#0)
> PATCH /welcome HTTP/1.1
> Host: cbmctf2019.cf:2002
> User-Agent: curl/7.64.1
> Accept: */*
> 
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Content-Type: text/html; charset=utf-8
< Content-Length: 99
< Server: Werkzeug/0.15.1 Python/2.7.13
< Date: Sun, 07 Apr 2019 17:49:41 GMT
< 
* Closing connection 0
nice!! Sometime task is simple but you made it complex... flag is cbmctf{PATCH_!s_4l50_@_r3qU3St!!}
```

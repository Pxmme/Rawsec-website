---
layout: post
title: "INS'hAck 2018 - Write-up"
lang: en
categories:
  - writeups
tags:
  - security
  - writeups
  - ctf
  - misc
date: 2018/04/08
thumbnail: /images/ctf.png
authorId: noraj
---
## Information

### Version

| By    | Version | Comment
| ---   | ---     | ---
| noraj | 1.0     | Creation

### CTF

- **Name** : INS'hAck 2018
- **Website** : [ctf.insecurity-insa.fr](http://ctf.insecurity-insa.fr/)
- **Type** : Online
- **Format** : Jeopardy
- **CTF Time** : [link](https://ctftime.org/event/592/)

## Self congratulation - Misc

> This website is pretty cool, right? I mean, we did a pretty good job esthetically and stuff, huh?
>
> Please submit as `INSA{insert string found here}

Embedded into the image we can see some kind of what looks like a barcode. But it isn't.

![](https://i.imgur.com/0mzTgcC.png)

Just replace white pixels with `0` and black pixels with `1` and you get: `001100010011001000110011001101000011010100110110001101110011100000`.

Let's use ruby to convert binary to ASCII:

```ruby
irb(main):019:0> ["001100010011001000110011001101000011010100110110001101110011100000"].pack("B*")
=> "12345678\x00"
```

Here is the flag: `INSA{12345678}`.

## Biography

**noraj** is my CTF and hacker pseudo. I'm a Cyber-Security engineer and ethical hacker. I created the Rawsec blog in 2012 and the Rawsec CTF Team in 2016.

## Activities

Here you can find each security contest that I participated in, including CTF (mostly), conferences or security exercises.

### 2018

{% spoiler "2018 events" %}
Contest                             | Date                                  | Location                  | Links
---                                 | ---                                   | ---                       | ---
SigSeg V1                           | Sat, 01 Dec. 2018 — Sun, 02 Dec. 2018 | Paris, IdF, France        | [website][sigsegv1]
Hack&#46;lu CTF 2018                | Tue, 16 Oct. 2018 — Thu, 18 Oct. 2018 | On-line                   | [CTFTime][ctftimeHacklu2018] - [website][hacklu]
picoCTF 2018                        | Fri, 28 Sep. 2018 — Fri, 12 Oct. 2018 | On-line                   | [CTFTime][ctftimePicoCTF2018] - [website][picoCTF]
SIGSEGv1 Quals 2018                 | Fri, 28 Sep. 2018 — Fri, 12 Oct. 2018 | On-line                   | [website][sigsegQuals]
DefCamp CTF Qualification 2018      | Sat, 22 Sep. 2018 — Sun, 23 Sep. 2018 | On-line                   | [CTFTime][ctftimeDefcamp2018quals] - [website][defcamp]
HackIT CTF 2018                     | Sat, 08 Sep. 2018 — Mon, 10 Sep. 2018 | On-line                   | [CTFTime][ctftimeHackit2018] - [website][hackit]
noxCTF 2018                         | Thu, 06 Sep. 2018 — Sat, 08 Sep. 2018 | On-line                   | [CTFTime][ctftimeNoxCTF2018] - [website][noxCTF2018]
TokyoWesterns CTF 4th 2018          | Sat, 01 Sep. 2018 — Mon, 03 Sep. 2018 | On-line                   | [CTFTime][ctftimeTokyowesterns2018] - [website][tokyowesterns2018]
Codefest CTF 2018                   | Fri, 31 Aug. 2018 — Sat, 01 Sep. 2018 | On-line                   | [CTFTime][ctftimeCodefest2018] - [website][codefest2018]
Hackcon 2018                        | Wed, 15 Aug. 2018 — Thu, 16 Aug. 2018 | On-line                   | [CTFTime][ctftimeHackcon2018] - [website][hackcon]
TJCTF 2018                          | Tue, 07 Aug. 2018 — Mon, 13 Aug. 2018 | On-line                   | [CTFTime][ctftimeTJCTF2018] - [website][tjctf]
CTFZone 2018 Quals                  | Sat, 21 Jul. 2018 — Sun, 22 Jul. 2018 | On-line                   | [CTFTime][ctftimeCTFZoneQuals2018] - [website][ctfzone]
Meepwn CTF Quals 2018               | Fri, 13 Jul. 2018 — Sun, 15 Jul. 2018 | On-line                   | [CTFTime][ctftimeMeePwnQuals2018] - [website][MeePwnCTF]
Google CTF 2018 Quals               | Sat, 23 Jun. 2018 — Mon, 25 Jun. 2018 | On-line                   | [CTFTime][ctftimeGoogleCTF2018] - [website][googlectf]
ASIS CTF Quals 2018                 | Sat, 28 Apr. 2018 — Mon, 30 Apr. 2018 | On-line                   | [CTFTime][ctftimeASISCTFquals2018] - [website][asisctf]
NoNameCon CTF Quals 2018            | Sat, 21 Apr. 2018 — Mon, 23 Apr. 2018 | On-line                   | [CTFTime][ctftimeNoNameConCTFQuals2018] - [website][NoNameConCTF]
BreizhCTF 2018                      | Fri, 20 Apr. 2018 — Sat, 21 Apr. 2018 | Rennes, Bretagne, France  | [website][BreizhCTF]
PACTF 2018                          | Mon, 16 Apr. 2018 — Mon, 30 Apr. 2018 | On-line                   | [CTFTime][ctftimePACTF2018] - [website][pactf]
Timisoara CTF 2018 Quals            | Sat, 14 Apr. 2018 — Sat, 21 Apr. 2018 | On-line                   | [CTFTime][ctftimeTimisoaraCTF2018Quals] - [website][timisoaractf]
WPICTF 2018                         | Fri, 13 Apr. 2018 — Sun, 15 Apr. 2018 | On-line                   | [CTFTime][ctftimeWPICTF2018] - [website][wpictf]
HITB-XCTF GSEC CTF 2018 Quals       | Wed, 11 Apr. 2018 — Fri, 13 Apr. 2018 | On-line                   | [CTFTime][ctftimeHITBXCTF2018quals] - [website][hitbxctf2018]
Byte Bandits CTF 2018               | Sat, 07 Apr. 2018 — Sun, 08 Apr. 2018 | On-line                   | [CTFTime][ctftimeByteBanditsCTF2018] - [website][ByteBanditsCTF]
UIUCTF 2018                         | Sat, 07 Apr. 2018 — Mon, 09 Apr. 2018 | On-line                   | [CTFTime][ctftimeUIUCTF2018] - [website][uiuctf]
Sunshine CTF 2018                   | Thu, 05 Apr. 2018 — Sat, 07 Apr. 2018 | On-line                   | [CTFTime][ctftimeSunshineCTF2018] - [website][sunshineCTF]
INS'hAck 2018                       | Thu, 05 Apr. 2018 — Sun, 08 Apr. 2018 | On-line                   | [CTFTime][ctftimeInshack2018] - [website][inshack]
0CTF 2018 Quals                     | Sat, 31 Mar. 2018 — Mon, 02 Apr. 2018 | On-line                   | [CTFTime][ctftime0CTFquals2018] - [website][0ctf]
Nuit du Hack CTF Quals 2018         | Fri, 30 Mar. 2018 — Sat, 31 Mar. 2018 | On-line                   | [CTFTime][ctftimeNDHquals2018] - [website][NDHquals]
SwampCTF 2018                       | Fri, 30 Mar. 2018 — Sun, 01 Apr. 2018 | On-line                   | [CTFTime][ctftimeSwampCTF2018] - [website][swamptctf]
Securinets CTF Quals 2018           | Sat, 24 Mar. 2018 — Sun, 25 Mar. 2018 | On-line                   | [CTFTime][ctftimeSecurinets2018] - [website][securiNets]
VolgaCTF 2018 Quals                 | Fri, 23 Mar. 2018 — Sun, 25 Mar. 2018 | On-line                   | [CTFTime][ctftimeVolgaCTF2018] - [website][volgaCTF2018]
BackdoorCTF 2018                    | Sat, 17 Mar. 2018 — Sun, 18 Mar. 2018 | On-line                   | [CTFTime][ctftimeBackdoorCTF2018] - [website][backdoorCTF2018]
angstromCTF 2018                    | Fri, 16 Mar. 2018 — Wed, 21 Mar. 2018 | On-line                   | [CTFTime][ctftimeAngstromCTF2018] - [website][angstromCTF]
N1CTF 2018                          | Sat, 10 Mar. 2018 — Mon, 12 Mar. 2018 | On-line                   | [CTFTime][ctftimeN1CTF2018] - [website][N1CTF]
Pragyan CTF 2018                    | Fri, 02 Mar. 2018 — Sat, 03 Mar. 2018 | On-line                   | [CTFTime][ctftimePragyanCTF2018] - [website][pragyanCTF]
NeverLAN CTF 2018                   | Fri, 23 Feb. 2018 — Tue, 27 Feb. 2018 | On-line                   | [CTFTime][ctftimeNeverLANCTF2018] - [website][NeverLANCTF]
TAMUctf 18                          | Sat, 17 Feb. 2018 — Mon, 26 Feb. 2018 | On-line                   | [CTFTime][ctftimeTAMUctf18] - [website][TAMUctf]
Harekaze CTF 2018                   | Sat, 10 Feb. 2018 — Sun, 11 Feb. 2018 | On-line                   | [CTFTime][ctftimeHarekaze2018] - [website][Harekaze]
EasyCTF IV                          | Sat, 10 Feb. 2018 — Tue, 20 Feb. 2018 | On-line                   | [CTFTime][ctftimeEasyCTF2018] - [website][EasyCTF]
nullcom HackIM 2018                 | Fri, 09 Feb. 2018 — Sun, 11 Feb. 2018 | On-line                   | [CTFTime][ctftimenullcomHackIM2018] - [website][nullcomHackIM]
EvlzCTF 2018                        | Fri, 09 Feb. 2018 — Sat, 10 Feb. 2018 | On-line                   | [CTFTime][ctftimeEvlzCTF] - [website][EvlzCTF]
SharifCTF 8                         | Fri, 02 Feb. 2018 — Sat, 03 Feb. 2018 | On-line                   | [CTFTime][ctftimeSharifCTF8] - [website][SharifCTF8]
FIC 2018                            | Tue, 23 Jan. 2018 — Wed, 24 Jan. 2018 | Lille, Hauts-de-France, France  | [website][FIC]

[sigsegv1]:https://sigsegv1.rtfm.re/
[ctftimeHacklu2018]:https://ctftime.org/event/699
[hacklu]:https://arcade.fluxfingers.net/
[ctftimePicoCTF2018]:https://ctftime.org/event/681
[picoCTF]:https://picoctf.com/
[sigsegQuals]:https://qual.rtfm.re/
[ctftimeDefcamp2018quals]:https://ctftime.org/event/654
[defcamp]:https://dctf.def.camp/
[ctftimeHackit2018]:https://ctftime.org/event/672
[hackit]:https://ctftime.org/event/672
[ctftimeNoxCTF2018]:https://ctftime.org/event/671
[noxCTF2018]:https://ctf18.noxale.com/
[ctftimeTokyowesterns2018]:https://ctftime.org/event/651
[tokyowesterns2018]:https://tokyowesterns.github.io/ctf2018/
[ctftimeCodefest2018]:https://ctftime.org/event/677
[codefest2018]:https://www.hackerrank.com/codefest-ctf-18
[ctftimeHackcon2018]:https://ctftime.org/event/652
[hackcon]:http://www.hackcon.in/
[ctftimeTJCTF2018]:https://ctftime.org/event/660
[tjctf]:https://tjctf.org/
[ctftimeCTFZoneQuals2018]:https://ctftime.org/event/632
[ctfzone]:https://ctf.bi.zone/
[ctftimeMeePwnQuals2018]:https://ctftime.org/event/625/
[MeePwnCTF]:https://ctf.meepwn.team/
[ctftimeGoogleCTF2018]:https://ctftime.org/event/623
[googlectf]:https://g.co/ctf
[ctftimeASISCTFquals2018]:https://ctftime.org/event/568
[asisctf]:https://asisctf.com/
[ctftimeNoNameConCTFQuals2018]:https://ctftime.org/event/616
[NoNameConCTF]:https://ctf.nonamecon.org/
[BreizhCTF]:https://www.breizhctf.com/
[ctftimePACTF2018]:https://ctftime.org/event/576
[pactf]:https://pactf.com/
[ctftimeTimisoaraCTF2018Quals]:https://ctftime.org/event/594
[timisoaractf]:https://www.timisoaractf.com/
[ctftimeWPICTF2018]:https://ctftime.org/event/600
[wpictf]:http://wpictf.xyz/
[ctftimeHITBXCTF2018quals]:https://ctftime.org/event/598
[hitbxctf2018]:https://hitbxctf2018.xctf.org.cn/
[ctftimeByteBanditsCTF2018]:https://ctftime.org/event/596
[ByteBanditsCTF]:https://ctf.euristica.in/
[ctftimeUIUCTF2018]:https://ctftime.org/event/587
[uiuctf]:https://sigpwny.github.io/ctf.html
[ctftimeSunshineCTF2018]:https://ctftime.org/event/593
[sunshineCTF]:https://sunshinectf.org/
[ctftimeInshack2018]:https://ctftime.org/event/592
[inshack]:https://ctf.insecurity-insa.fr/
[ctftime0CTFquals2018]:https://ctftime.org/event/557
[0ctf]:https://ctf.0ops.sjtu.cn/
[ctftimeNDHquals2018]:https://ctftime.org/event/583
[NDHquals]:https://quals.nuitduhack.com/
[ctftimeSwampCTF2018]:https://ctftime.org/event/590
[swamptctf]:https://swampctf.com/
[ctftimeSecurinets2018]:https://ctftime.org/event/588
[securiNets]:https://www.ctfsecurinets.com/
[ctftimeVolgaCTF2018]:https://ctftime.org/event/539
[volgaCTF2018]:https://quals.2018.volgactf.ru/
[ctftimeBackdoorCTF2018]:https://ctftime.org/event/585
[backdoorCTF2018]:https://backdoor.sdslabs.co/competitions/backdoorctf18/dashboard
[ctftimeAngstromCTF2018]:https://ctftime.org/event/577
[angstromCTF]:https://www.angstromctf.com/
[ctftimeN1CTF2018]:https://ctftime.org/event/584
[N1CTF]:http://n1ctf.xctf.org.cn/
[ctftimePragyanCTF2018]:https://ctftime.org/event/581
[pragyanCTF]:https://ctf.pragyan.org/
[ctftimeNeverLANCTF2018]:https://ctftime.org/event/569
[NeverLANCTF]:http://neverlanctf.com/
[ctftimeTAMUctf18]:https://ctftime.org/event/559
[TAMUctf]:https://ctf.tamu.edu/
[ctftimeHarekaze2018]:https://ctftime.org/event/549
[Harekaze]:https://harekaze.com/ctf.html
[ctftimeEasyCTF2018]:https://ctftime.org/event/573
[EasyCTF]:https://www.easyctf.com/
[ctftimenullcomHackIM2018]:https://ctftime.org/event/566
[nullcomHackIM]:http://ctf.nullcon.net/
[ctftimeEvlzCTF]:https://ctftime.org/event/570
[EvlzCTF]:https://evlzctf.in/
[ctftimeSharifCTF8]:https://ctftime.org/event/507
[SharifCTF8]:http://ctf.certcc.ir/
[FIC]:https://www.forum-fic.com/
{% endspoiler %}

### 2017

I participated in 61 security events in 2017.

{% spoiler "2017 events" %}
Contest                             | Date                                  | Location                  | Links
---                                 | ---                                   | ---                       | ---
Christmas Challenge 2017            | Sun, 24 Dec. 2017 — Tue, 26 Dec. 2017 | On-line                   | [website][SHX]
3DSCTF 2017                         | Fri, 15 Dec. 2017 — Mon, 18 Dec. 2017 | On-line                   | [CTFTime][ctft3DSCTF2017] - [website][3DSCTF]
InCTF 2017                          | Sat, 16 Dec. 2017 — Sun, 17 Dec. 2017 | On-line                   | [CTFTime][ctftimeInctf2017] - [website][inctf]
SECCON 2017 Online CTF              | Sat, 09 Dec. 2017 — Sun, 10 Dec. 2017 | On-line                   | [CTFTime][ctftimeSeccon2017] - [website][seccon2017]
Shellter Hacking Express 18         | Sun, 03 Dec. 2017 — Sun, 03 Dec. 2017 | On-line                   | [CTFTime][ctftimeSHX18] - [website][SHX]
Takoma Park CTF                     | Sat, 02 Dec. 2017 — Mon, 04 Dec. 2017 | On-line                   | [CTFTime][ctftimeTpctf2017] - [website][tpctf]
Mini CTF 2017 - Riseup              | Wed, 29 Nov. 2017 — Thu, 30 Nov. 2017 | On-line                   | [website][minictfriseup]
TUCTF 2017                          | Fri, 24 Nov. 2017 — Sun, 26 Nov. 2017 | On-line                   | [CTFTime][ctftimeTuctf2017] - [website][tuctf]
RC3 CTF 2017                        | Sat, 18 Nov. 2017 — Mon, 20 Nov. 2017 | On-line                   | [CTFTime][ctftimeRC3CTF2017] - [website][rc3ctf]
hxp CTF 2017                        | Fri, 17 Nov. 2017 — Sun, 19 Nov. 2017 | On-line                   | [CTFTime][ctftimeHXPCTF2017] - [website][hxpctf]
RUSecure-CTF 2017 Preparation Round | Sat, 04 Nov. 2017 — Thu, 07 Dec. 2017 | On-line                   | [CTFTime][ctftimeRuctfQuals2017] - [website][ructf]
HITCON CTF 2017 Quals               | Sat, 04 Nov. 2017 — Mon, 06 Nov. 2017 | On-line                   | [CTFTime][ctftimeHitconQuals2017] - [website][hitcon]
Oman National Cyber Security CTF 2017 Quals | Thu, 26 Oct. 2017 — Sun, 29 Oct. 2017 | On-line           | [CTFTime][ctftimeOmanNationalCyberSecurityCTF2017] - [website][OmanNationalCyberSecurityCTF2017]
Pwn2Win CTF 2017                    | Fri, 20 Oct. 2017 — Sun, 22 Oct. 2017 | On-line                   | [CTFTime][ctftimePwn2Win2017] - [website][pwn2win]
Hack&#46;lu CTF 2017                | Tue, 17 Oct. 2017 — Thu, 19 Oct. 2017 | On-line                   | [CTFTime][ctftimeHacklu2017] - [website][hacklu2017]
European Cyber Week CTF Quals 2017  | Fri, 06 Oct. 2017 — Sun, 22 Oct. 2017 | Online                    | [website][challengeECW]
Hack Dat Kiwi 2017                  | Fri, 13 Oct. 2017 — Sun, 15 Oct. 2017 | On-line                   | [CTFTime][ctftimeHackDatKiwi2017] - [website][hackdatkiwi]
Kaspersky Industrial CTF Quals 2017 | Fri, 06 Oct. 2017 — Sun, 08 Oct. 2017 | On-line                   | [CTFTime][ctftimeKasperskyCtf2017] - [website][kasperskyctf]
Square CTF 2017                     | Wed, 04 Oct. 2017 — Sat, 14 Oct. 2017 | On-line                   | [CTFTime][ctftimeSquarectf2017] - [website][squarectf]
DefCamp CTF Qualification 2017      | Sat, 30 Sep. 2017 — Sun, 01 Oct. 2017 | On-line                   | [CTFTime][ctftimeDefcampQuals2017] - [website][defcamp]
HackCon 2017                        | Fri, 25 Aug. 2017 — Sat, 26 Aug. 2017 | On-line                   | [CTFTime][ctftimeHackcon2017] - [website][hackcon]
HackIT CTF 2017                     | Fri, 25 Aug. 2017 — Sun, 27 Aug. 2017 | On-line                   | [CTFTime][ctftimeHackit2017] - [website][hackit]
HITB CTF Singapore 2017             | Thu, 24 Aug. 2017 — Fri, 25 Aug. 2017 | On-line                   | [CTFTime][ctftimeHitb2017] - [website][hitb]
CTFZone 2017                        | Sat, 15 Jul. 2017 — Sun, 16 Jul. 2017 | On-line                   | [CTFTime][ctftimeCtfzone2017] - [website][ctfzone]
MeePwn CTF 1st 2017                 | Fri, 14 Jul. 2017 — Sun, 16 Jul. 2017 | On-line                   | [CTFTime][ctftimeMeepwn2017] - [website][meepwn]
PoliCTF 2017                        | Fri, 07 Jul. 2017 — Sun, 09 Jul. 2017 | On-line                   | [CTFTime][ctftimePoliCTF2017] - [website][polictf]
SECUINSIDE CTF Quals 2017           | Sat, 01 Jul. 2017 — Sun, 02 Jul. 2017 | On-line                   | [CTFTime][ctftimeSECUINSIDECTFQuals2017] - [website][SECUINSIDECTF2017]
Google CTF 2017 Quals               | Sat, 17 Jun. 2017 — Sun, 18 Jun. 2017 | On-line                   | [CTFTime][ctftimeGoogleCTF2017quals] - [website][googleCTF]
LabyREnth 2017                      | Fri, 09 Jun. 2017 — Sun, 23 Jul. 2017 | On-line                   | [CTFTime][ctftimeLabyrenth2017] - [website][labyrenth]
SHA2017 CTF Teaser round            | Sat, 10 Jun. 2017 — Sat, 10 Jun. 2017 | On-line                   | [CTFTime][ctftimeSHA2017ctf] - [website][sha2017ctf]
SSTIC2017                           | Wed, 07 Jun. 2016 — Fri, 09 Jun. 2017 | Streaming                 | [website][sstic2017]
RCTF 2017                           | Sat, 20 May  2017 — Mon, 22 May  2017 | On-line                   | [CTFTime][ctftimeRctf2017] - [website][rctf]
UIUCTF 2017                         | Sat, 29 Apr. 2017 — Sun, 30 Apr. 2017 | On-line                   | [CTFTime][ctftimeUiuctf2017] - [website][uiuctf]
BreizhCTF 2k17                      | Fri, 28 Apr. 2017 — Sat, 29 Apr. 2017 | Rennes, Bretagne, France  | [website][breizhctf]
YUBITSEC CTF 2017                   | Sat, 22 Apr. 2017 — Mon, 24 Apr. 2017 | On-line                   | [CTFTime][ctftimeYubitsecCtf2017] - [website][yubitsecCtf]
angstromCTF 2017                    | Sat, 22 Apr. 2017 — Thu, 27 Apr. 2017 | On-line                   | [CTFTime][ctftimeAngstromctf2017] - [website][angstromctf]
PlaidCTF 2017                       | Fri, 21 Apr. 2017 — Sun, 23 Apr. 2017 | On-line                   | [CTFTime][ctftimePlaidctf2017] - [website][plaidctf]
Insigne CTF 2017                    | Fri, 21 Apr. 2017 — Sat, 22 Apr. 2017 | On-line                   | [CTFTime][ctftimeInsignectf2017] - [website][insignectf]
PACTF 2017                          | Sun, 16 Apr. 2017 — Thu, 05 Jan. 2017 | On-line                   | [CTFTime][ctftimePacctf2017] - [website][pactf]
TAMUctf 2017                        | Wed, 12 Apr. 2017 — Sun, 23 Apr. 2017 | On-line                   | [CTFTime][ctftimeTAMUctf2017] - [website][TAMUctf]
FIT-HACK CTF 2017                   | Sat, 08 Apr. 2017 — Sat, 15 Apr. 2017 | On-line                   | [CTFTime][ctftimeFithackctf2017] - [website][fithackctf]
Sunshine CTF 2017                   | Fri, 07 Apr. 2017 — Sat, 08 Apr. 2017 | On-line                   | [CTFTime][ctftimeSunshinectf2017] - [website][sunshinectf]
ASIS CTF Quals 2017                 | Fri, 07 Apr. 2017 — Sun, 09 Apr. 2017 | On-line                   | [CTFTime][ctftimeAsisctf2017] - [website][asisctf]
INS'hAck 2017                       | Thu, 06 Apr. 2017 — Sun, 09 Apr. 2017 | On-line                   | [CTFTime][ctftimeINShAck2017] - [website][INShAck]
Egypt & UAE National Cyber Security CTF Quals 2017  | Thu, 06 Apr. 2017 — Sat, 08 Apr. 2017 | On-site   | [CTFTime][ctftimeEgyptUAENationalCyberSecurityCTFquals2017] - [website][EgyptUAENationalCyberSecurityCTF]
picoCTF 2017                        | Fri, 31 Mar. 2017 — Fri, 14 Apr. 2017 | On-line                   | [CTFTime][picoCTF2017] - [website][ctftimepicoCTF2017]
Nuit du Hack CTF Quals 2017         | Fri, 31 Mar. 2017 — Sat, 01 Apr. 2017 | On-line                   | [CTFTime][ctftimeNDHquals2017] - [website][qualsNDH]
DEFNET 2017                         | Thu, 30 Mar. 2017 — Thu, 30 Mar. 2017 | Rennes, Bretagne, France  | [event][defnet2017]
DoubleS1405 CTF                     | Sat, 25 Mar. 2017 — Sun, 26 Mar. 2017 | On-line                   | [CTFTime][ctftimeDoubleS1405CTF] - [website][DoubleS1405CTF]
WhiteHat Challenge 02               | Sat, 25 Mar. 2017 — Sat, 25 Mar. 2017 | On-line                   | [CTFTime][ctftimeWhiteHatChallenge02] - [website][whitehat]
VolgaCTF 2017 Quals                 | Fri, 24 Mar. 2017 — Sun, 26 Mar. 2017 | On-line                   | [CTFTime][ctftimeVolgaCTFQuals2017] - [website][volgactfQuals2017]
0CTF 2017 Quals                     | Sat, 18 Mar. 2017 — Mon, 20 Mar. 2017 | On-line                   | [CTFTime][ctftime0CTF2017] - [website][0ctf]
EasyCTF 2017                        | Mon, 13 Mar. 2017 — Mon, 20 Mar. 2017 | On-line                   | [CTFTime][ctftimeEasyCTF2017] - [website][easyctf]
Pragyan CTF 2017                    | Thu, 02 Mar. 2017 — Tue, 07 Mar. 2017 | On-line                   | [CTFTime][ctftimePragyanCTF2017] - [website][pragyanCTF]
Xiomara CTF 2017                    | Sat, 25 Feb. 2017 — Mon, 27 Feb. 2017 | On-line                   | [CTFTime][ctftimeXiomaraCTF2017] - [website][XiomaraCTF]
WhiteHat Challenge 01               | Sat, 25 Feb. 2017 — Sat, 25 Feb. 2017 | On-line                   | [CTFTime][ctftimeWhitehatChallenge2017] - [website][whitehat]
NeverLAN CTF 2017                   | Fri, 24 Feb. 2017 — Mon, 27 Feb. 2017 | On-line                   | [CTFTime][ctftimeNeverlanCTF2017] - [website][neverlanCTF]
BSides San Francisco CTF 2017       | Sun, 12 Feb. 2017 — Tue, 14 Feb. 2017 | On-line                   | [CTFTime][ctftimebsidesSanFrancisco2017] - [website][bsidesSanFrancisco]
BITSCTF 2017                        | Sat, 04 Feb. 2017 — Sun, 05 Feb. 2017 | On-line                   | [CTFTime][ctftimeBitsctf2017] - [website][bitsctf]
AlexCTF 2017                        | Fri, 03 Feb. 2017 — Mon, 06 Feb. 2017 | On-line                   | [CTFTime][ctftimeAlexctf2017] - [website][alexctf]
Insomni'hack teaser 2017            | Sat, 21 Jan. 2017 — Sun, 22 Jan. 2017 | On-line                   | [CTFTime][ctftimeInsomnihackTeaser2017] - [website][insomnihackTeaser]

[ctft3DSCTF2017]:https://ctftime.org/event/547
[3DSCTF]:https://3dsctf.org/
[ctftimeInctf2017]:https://ctftime.org/event/551
[inctf]:https://ctf.inctf.in/
[ctftimeSeccon2017]:https://ctftime.org/event/512
[seccon2017]:https://score-quals.seccon.jp/
[ctftimeSHX18]:https://ctftime.org/event/533
[SHX]:https://shellterlabs.com/en/contests/
[ctftimeTpctf2017]:https://ctftime.org/event/535
[tpctf]:https://tpctf.com/
[minictfriseup]:https://cybertalents.com/competitions/23
[ctftimeTuctf2017]:https://ctftime.org/event/500
[tuctf]:https://tuctf.asciioverflow.com/
[ctftimeRC3CTF2017]:https://ctftime.org/event/529
[rc3ctf]:https://rc3ctf.rc3.club/
[ctftimeHXPCTF2017]:https://ctftime.org/event/489
[hxpctf]:https://ctf.hxp.io/
[ctftimeRuctfQuals2017]:https://ctftime.org/event/534
[ructf]:http://ructfbox.com/
[ctftimeHitconQuals2017]:https://ctftime.org/event/485
[hitcon]:http://ctf.hitcon.org/
[ctftimeOmanNationalCyberSecurityCTF2017]:https://ctftime.org/event/523
[OmanNationalCyberSecurityCTF2017]:https://cybertalents.com/oman-ctf-2017
[ctftimePwn2Win2017]:https://ctftime.org/event/427
[pwn2win]:https://www.pwn2win.party/
[ctftimeHacklu2017]:https://ctftime.org/event/525
[hacklu2017]:https://flatearth.fluxfingers.net/register
[challengeECW]:https://challenge-ecw.fr/
[ctftimeHackDatKiwi2017]:https://ctftime.org/event/516
[hackdatkiwi]:https://hack.dat.kiwi/
[ctftimeKasperskyCtf2017]:https://ctftime.org/event/519
[kasperskyctf]:https://ctf.kaspersky.com/
[ctftimeSquarectf2017]:https://ctftime.org/event/518
[squarectf]:https://squarectf.com/
[ctftimeDefcampQuals2017]:https://ctftime.org/event/484
[defcamp]:https://dctf.def.camp/
[ctftimeHackcon2017]:https://ctftime.org/event/481
[hackcon]:http://hackcon.in/
[ctftimeHackit2017]:https://ctftime.org/event/491
[hackit]:https://ctf.com.ua/
[ctftimeHitb2017]:https://ctftime.org/event/498
[hitb]:https://hitb.xctf.org.cn/
[ctftimeCtfzone2017]:https://ctftime.org/event/476
[ctfzone]:https://ctf.bi.zone/
[ctftimeMeepwn2017]:https://ctftime.org/event/486
[meepwn]:https://ctf.meepwn.team/
[ctftimePoliCTF2017]:https://ctftime.org/event/425
[polictf]:http://www.polictf.it/
[ctftimeSECUINSIDECTFQuals2017]:https://ctftime.org/event/470
[SECUINSIDECTF2017]:http://secuinside.com/2017/ctf.html
[ctftimeGoogleCTF2017quals]:https://ctftime.org/event/455
[googleCTF]:https://capturetheflag.withgoogle.com/
[ctftimeLabyrenth2017]:https://ctftime.org/event/471
[labyrenth]:http://labyrenth.com/
[ctftimeSHA2017ctf]:https://ctftime.org/event/479
[sha2017ctf]:https://ctf.sha2017.org/
[sstic2017]:https://www.sstic.org/2017/programme/
[ctftimeRctf2017]:https://ctftime.org/event/450
[rctf]:http://ctf.teamrois.cn/
[ctftimeUiuctf2017]:https://ctftime.org/event/453
[uiuctf]:https://sigpwny.github.io/ctf.html
[breizhctf]:http://www.breizhctf.com/
[ctftimeYubitsecCtf2017]:https://ctftime.org/event/464
[yubitsecCtf]:http://ctf.yubitsec.org/
[ctftimeAngstromctf2017]:https://ctftime.org/event/435
[angstromctf]:https://www.angstromctf.com/
[ctftimePlaidctf2017]:https://ctftime.org/event/439
[plaidctf]:http://plaidctf.com/
[ctftimeInsignectf2017]:https://ctftime.org/event/462
[insignectf]:http://insignectf.in/
[ctftimePacctf2017]:https://ctftime.org/event/438
[pactf]:https://pactf.com/
[ctftimeTAMUctf2017]:https://ctftime.org/event/461
[TAMUctf]:http://ctf.tamu.edu/
[ctftimepicoCTF2017]:https://ctftime.org/event/443
[picoCTF2017]:https://2017.picoctf.com/
[ctftimeFithackctf2017]:https://ctftime.org/event/451
[fithackctf]:https://ctf.nw.fit.ac.jp/
[ctftimeSunshinectf2017]:https://ctftime.org/event/449
[sunshinectf]:https://sunshinectf.org/
[ctftimeAsisctf2017]:https://ctftime.org/event/364
[asisctf]:http://asis-ctf.ir/
[ctftimeINShAck2017]:https://ctftime.org/event/444
[INShAck]:http://ctf.insecurity-insa.fr/
[ctftimeEgyptUAENationalCyberSecurityCTFquals2017]:https://ctftime.org/event/445
[EgyptUAENationalCyberSecurityCTF]:http://www.cybertalents.com/
[ctftimeNDHquals2017]:https://ctftime.org/event/410
[qualsNDH]:https://quals.nuitduhack.com/
[defnet2017]:http://www.defense.gouv.fr/salle-de-presse/dossiers-de-presse/dossier-de-presse-defnet-2017
[ctftimeDoubleS1405CTF]:https://ctftime.org/event/446
[DoubleS1405CTF]:http://doubles1405.com/
[ctftimeWhiteHatChallenge02]:https://ctftime.org/event/447
[whitehat]:https://wargame.whitehat.vn/
[ctftimeVolgaCTFQuals2017]:https://ctftime.org/event/374
[volgactfQuals2017]:https://quals.2017.volgactf.ru/
[ctftime0CTF2017]:https://ctftime.org/event/402
[0ctf]:https://ctf.0ops.net/
[ctftimeEasyCTF2017]:https://ctftime.org/event/441
[easyctf]:https://www.easyctf.com/
[ctftimePragyanCTF2017]:https://ctftime.org/event/420
[pragyanCTF]:https://ctf.pragyan.org/home
[ctftimeXiomaraCTF2017]:https://ctftime.org/event/430
[XiomaraCTF]:https://xiomara.xyz/
[ctftimeWhitehatChallenge2017]:https://ctftime.org/event/429
[ctftimeNeverlanCTF2017]:https://ctftime.org/event/431
[neverlanCTF]:https://neverlanctf.com/
[ctftimebsidesSanFrancisco2017]:https://ctftime.org/event/414
[bsidesSanFrancisco]:https://scoreboard.ctf.bsidessf.com/
[ctftimeBitsctf2017]:https://ctftime.org/event/417
[bitsctf]:https://bitsctf.bits-quark.org/
[ctftimeAlexctf2017]:https://ctftime.org/event/408/
[alexctf]:https://ctf.oddcoder.com/
[ctftimeInsomnihackTeaser2017]:https://ctftime.org/event/382
[insomnihackTeaser]:https://teaser.insomnihack.ch/
{% endspoiler %}

### 2016

I participated in 23 security events in 2016.

{% spoiler "2016 events" %}
Contest                             | Date                                  | Location                  | Links
---                                 | ---                                   | ---                       | ---
33C3 CTF                            | Tue, 27 Dec. 2016 — Thu, 29 Dec. 2016 | On-line                   | [CTFTime][ctftime33c3ctf2016] - [website][33c3ctf]
WhiteHat Grand Prix 2016            | Sat, 17 Dec. 2016 — Sun, 18 Dec. 2016 | On-line                   | [CTFTime][ctftimeWhitehatGrandPrix2016] - [website][whitehatGrandPrix2016]
SECCON 2016 Online CTF              | Sat, 10 Dec. 2016 — Sun, 11 Dec. 2016 | On-line                   | [CTFTime][ctftimeSeccon2016] - [website][seccon2016]
Juniors CTF 2016                    | Fri, 25 Nov. 2016 — Sun, 27 Nov. 2016 | On-line                   | [CTFTime][ctftimeJuniorsCTF2016] - [website][juniorsCTF]
RC3 CTF 2016                        | Sat, 19 Nov. 2016 — Mon, 21 Nov. 2016 | On-line                   | [CTFTime][ctftimeRC3CTF2016] - [website][RC3CTF]
Qiwi-Infosec CTF-2016               | Thu, 17 Nov. 2016 — Fri, 18 Nov. 2016 | On-line                   | [CTFTime][ctftimeQiwiCTF2016] - [website][qiwictf]
School CTF 2016                     | Sun, 06 Nov. 2016 — Sun, 06 Nov. 2016 | On-line                   | [CTFTime][ctftimeSchoolCTF2016] - [website][SchoolCTF]
Hack The Vote 2016                  | Sat, 05 Nov. 2016 — Mon, 07 Nov. 2016 | On-line                   | [CTFTime][ctftimeHackTheVote2016] - [website][HackTheVote]
European Cyber Week CTF Quals 2016  | Fri, 21 Oct. 2016 — Sun, 06 Nov. 2016 | On-line                   | [website][ecw]
Hack&#46;lu CTF 2016                | Wed, 19 Oct. 2016 — Thu, 20 Oct. 2016 | On-line                   | [CTFTime][ctftimeHack.lu2016] - [website][hack.lu]
HITCON CTF 2016 Quals               | Sat, 08 Oct. 2016 — Mon, 10 Oct. 2016 | On-line                   | [CTFTime][ctftimeHitcon2016] - [website][hitcon]
HackIt CTF 2016 Online              | Fri, 23 Sep. 2016 — Sun, 02 Oct. 2016 | On-line                   | [CTFTime][ctftimeha2016] - [website][hackit]
CSAW CTF Qualification Round 2016   | Sat, 17 Sep. 2016 — Mon, 19 Sep. 2016 | On-line                   | [CTFTime][ctftimeCsaw2016] - [website][csaw]
WhiteHat Contest 12                 | Sat, 10 Sep. 2016 — Sun, 11 Sep. 2016 | On-line                   | [CTFTime][ctftimeWhitehatContest12] - [website][whitehat]
ASIS CTF Finals 2016                | Fri, 09 Sep. 2016 — Sun, 11 Sep. 2016 | On-line                   | [CTFTime][ctftimeAsisctf2016] - [website][asisctf]
CTF(x) 2016                         | Fri, 26 Aug. 2016 — Sun, 28 Aug. 2016 | On-line                   | [CTFTime][ctftimeCtfx2016] - [website][ctfx]
BioTerra CTF 2016                   | Sat, 20 Aug. 2016 — Sun, 21 Aug. 2016 | On-line                   | [CTFTime][ctftimeBioterra2016] - [website][bioterra]
Hackcon 2016                        | Fri, 19 Aug. 2016 — Sat, 20 Aug. 2016 | On-line                   | [CTFTime][ctftimeHackcon2016] - [website][hackcon]
IceCTF 2016                         | Fri, 12 Aug. 2016 — Fri, 26 Aug. 2016 | On-line                   | [CTFTime][ctftimeIcectf2016] - [website][icectf]
ABCTF 2016                          | Fri, 15 Jul. 2016 — Fri, 22 Jul. 2016 | On-line                   | [CTFTime][ctftimeAbctf2016] - [website][abctf]
BackdoorCTF 2016                    | Sat, 04 Jun. 2016 — Sun, 05 Jun. 2016 | On-line                   | [CTFTime][ctftimeBackdoorctf2016] - [website][backdoorctf]
SSTIC2016                           | Wed, 01 Jun. 2016 — Fri, 03 Jun. 2016 | Rennes, Bretagne, France  | [website][sstic2016]
BreizhCTF 2k16                      | Fri, 29 Apr. 2016 — Sat, 30 Apr. 2016 | Rennes, Bretagne, France  | [website][breizhctf]

[ctftime33c3ctf2016]:https://ctftime.org/event/404
[33c3ctf]:https://33c3ctf.ccc.ac/
[ctftimeWhitehatGrandPrix2016]:https://ctftime.org/event/398
[whitehatGrandPrix2016]:https://grandprix.whitehatvn.com/Contests/ChallengesContest/32
[ctftimeSeccon2016]:https://ctftime.org/event/354/
[seccon2016]:http://2016.seccon.jp/news/#124
[ctftimeJuniorsCTF2016]:https://ctftime.org/event/391
[juniorsCTF]:https://juniors.ctf.org.ru/
[ctftimeRC3CTF2016]:https://ctftime.org/event/389
[RC3CTF]:http://ctf.rc3.club/
[ctftimeQiwiCTF2016]:https://ctftime.org/event/385
[qiwictf]:https://qiwictf.ru/
[ctftimeSchoolCTF2016]:https://ctftime.org/event/384
[SchoolCTF]:https://school-ctf.org/
[ctftimeHackTheVote2016]:https://ctftime.org/event/345
[HackTheVote]:https://pwn.voting/
[ecw]:https://challenge-ecw.fr/
[ctftimeHack.lu2016]:https://ctftime.org/event/380
[hack.lu]:https://2016.hack.lu/ctf/
[ctftimeHitcon2016]:https://ctftime.org/event/355
[hitcon]:http://ctf.hitcon.org/
[ctftimeha2016]:https://ctftime.org/event/360
[hackit]:https://ctf.com.ua/
[ctftimeCsaw2016]:https://ctftime.org/event/347
[csaw]:https://ctf.csaw.io/
[ctftimeWhitehatContest12]:https://ctftime.org/event/357
[whitehat]:https://wargame.whitehat.vn/
[ctftimeAsisctf2016]:https://ctftime.org/event/327
[asisctf]:http://asis-ctf.ir/
[ctftimeCtfx2016]:https://ctftime.org/event/348/
[ctfx]:http://ctfx.io/
[ctftimeBioterra2016]:https://ctftime.org/event/350
[bioterra]:https://bioterra.xyz/
[ctftimeHackcon2016]:https://ctftime.org/event/341
[hackcon]:http://hackcon.in/
[ctftimeIcectf2016]:https://ctftime.org/event/319
[icectf]:https://icec.tf/
[ctftimeAbctf2016]:https://ctftime.org/event/333
[abctf]:https://abctf.xyz/
[ctftimeBackdoorctf2016]:https://ctftime.org/event/314
[backdoorctf]:https://backdoor.sdslabs.co/
[sstic2016]:https://www.sstic.org/2016/programme/
[breizhctf]:http://www.breizhctf.com/
{% endspoiler %}

### 2015

I participated in 1 security event in 2015.

{% spoiler "2015 events" %}
Contest             | Date                                  | Location                  | Links
---                 | ---                                   | ---                       | ---
DEFNET 2015         | April 2015                            | Rennes, Bretagne, France  | [event][defnet2015]

[defnet2015]:http://www.defense.gouv.fr/ema/actualites/defnet-2015-objectif-atteint
{% endspoiler %}

## Find me online


[CTFtime](https://ctftime.org/user/16344) | [Root-Me](https://www.root-me.org/alex-29004) | [GitHub](https://github.com/noraj) | [GitLab](https://gitlab.com/u/noraj) | [HackTheBox](https://www.hackthebox.eu/profile/32519) | [NewbieContest](https://www.newbiecontest.org/forums/index.php?action=profile;u=83818)
